﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyText : MonoBehaviour
{
    Text text;
    ResourceHandler resourceHandler;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
       
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Energy:" + GameManager.instance.resourceHandler.Energy;
    }
}
