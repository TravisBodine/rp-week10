﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceHandler : MonoBehaviour
{
    public int Energy = 0;

    public float baseEnergyRegainRate;
    public OrbitHandler orbitHandler;

    public AudioClip CantBuySound;

    [HideInInspector]
    public float regainRate;

    AudioSource auds;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        auds = GetComponent<AudioSource>();
        GameManager.instance.resourceHandler = this;
        regainRate = baseEnergyRegainRate;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= 1 / regainRate)
        {
            Energy++;
            timer = 0;
        }
    }

    public void BuyItem(int index)
    {
        OrbitObject objectToBuy = orbitHandler.SpawnAbleObjects[index];

        if(objectToBuy.cost <= Energy)
        {
            if (orbitHandler.AddNewOrbitItem(objectToBuy))
            {
                Energy -= objectToBuy.cost;
                return;
            }
            
        }

        auds.clip = CantBuySound;
        auds.Play();

    }
}
