﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitHandler : MonoBehaviour
{
    //public int layer1MaxObjects;
    //public int layer2MaxObjects;
    //public int layer3MaxObjects;
    public GameObject orbiter;
   
    public float OrbitSpeed;

    public List< OrbitObject> SpawnAbleObjects;
    
    public List<GameObject> layer1position;
    public List<GameObject> layer2position;
    public List<GameObject> layer3position;

    public List<OrbitObject> layer1Object;
    public List<OrbitObject> layer2Object;
    public List<OrbitObject> layer3Object;

    //public float layerSeperationDistance;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.resourceHandler.orbitHandler = this;
      
    }

    // Update is called once per frame
    void Update()
    {
        orbiter.transform.Rotate(GameManager.instance.playerController.GetComponent<Transform>().up, OrbitSpeed * Time.deltaTime);
    }

    public bool AddNewOrbitItem(OrbitObject objectToSpawn)
    {
        List<GameObject> layerPosition;
        List<OrbitObject> layerObject;
        int layerindex;

        if (layer1Object.Count< layer1position.Count)
        {
            layerPosition = layer1position;
            layerObject = layer1Object;
            layerindex = 0;
            
        }
        else if (layer2Object.Count < layer2position.Count)
        {
            layerPosition = layer2position;
            layerObject = layer2Object;
            layerindex = 1;
        }
        else if (layer3Object.Count < layer3position.Count)
        {
            layerPosition = layer3position;
            layerObject = layer3Object;
            layerindex = 2;
        }
        else
        {
            return false;
        }

        int index = layerObject.Count;
        //this kinda fixes it but dosent work if you build and destroy 2
        foreach (OrbitObject item in layerObject)
        {
            if(item.layerIndex == index)
            {
                index++;
            }
        }
    
        Transform target = layerPosition[index].transform;

        OrbitObject newOrbitObject = Instantiate<OrbitObject>(objectToSpawn,target.position,Quaternion.identity);

        newOrbitObject.target = target;
        newOrbitObject.orbitHandler = this;
        newOrbitObject.layerIndex = layerindex;
        layerObject.Add(newOrbitObject);
        return true;
    }
}
