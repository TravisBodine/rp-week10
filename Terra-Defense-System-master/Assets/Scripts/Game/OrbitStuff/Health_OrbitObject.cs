﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health_OrbitObject : Health
{
    public GameObject deathParticle;

    private AudioSource soundMaker;
    
    public AudioClip dieClip;

    protected override void Start()
    {     
        soundMaker = GetComponent<AudioSource>();
        base.Start();
    }
    public override void reduceHealth(int healthToRemove)
    {
        Instantiate(deathParticle, transform.position, transform.rotation);
        soundMaker.clip = dieClip;
        soundMaker.Play();
        Destroy(gameObject);
    }
}
