﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefensePlatform : OrbitObject
{
    public float fireRate;

    public float lineFadeTime;
    LineRenderer lineRenderer;

    float timer=0;

    public override void Start()
    {
        base.Start();
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
    }
    public override void Update()
    {
        
        base.Update();
      
        FireGun();
    }

    private void FireGun()
    {
        if(timer <= Time.time)
        {
            Enemy closestEnemy = null;
            foreach (Enemy enemy in GameManager.instance.enemies)
            {
                if (closestEnemy == null || Vector3.Distance(enemy.GetComponent<Transform>().position, tf.position) < Vector3.Distance(closestEnemy.GetComponent<Transform>().position, tf.position))
                {
                    closestEnemy = enemy;
                }


            }

            Debug.Log(closestEnemy);
            if(closestEnemy != null)
            {
                Vector3[] pos = { tf.position, closestEnemy.GetComponent<Transform>().position };
               StartCoroutine( DrawLine(pos));
                closestEnemy.gameObject.GetComponent<Health>().reduceHealth(1);
            }

            timer = Time.time + fireRate;
        }
      
    }



    IEnumerator DrawLine(Vector3[] pos)
    {
        lineRenderer.enabled = true;
        lineRenderer.SetPositions(pos);

        float countDown = lineFadeTime;

        while (countDown >=0)
        {
            
            countDown -= Time.deltaTime;
            yield return null;
        }
        lineRenderer.enabled = false;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
}
