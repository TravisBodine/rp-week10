﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitLayerWiggler : MonoBehaviour
{
    public float MaxWiggle;
    public float WiggleSpeed;
    public bool upFirst;

    Transform tf;

   

    float totalAngle = 0;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();

        if (!upFirst)
        {
            WiggleSpeed *= -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        
        Vector3 euler = new Vector3(0, 0, WiggleSpeed * Time.deltaTime);
        tf.Rotate(euler,Space.Self);

        totalAngle += WiggleSpeed * Time.deltaTime;
        

        if (Mathf.Abs(totalAngle) >= MaxWiggle)
        {
            WiggleSpeed *= -1;
            totalAngle = 0;
        }
     
    }
}
