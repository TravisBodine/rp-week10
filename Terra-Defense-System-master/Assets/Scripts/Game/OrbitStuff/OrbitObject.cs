﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OrbitObject : MonoBehaviour
{
    protected Transform tf;

    public int cost;

    public Transform target;

    [HideInInspector]
    public OrbitHandler orbitHandler;
    [HideInInspector]
    public int layerIndex;
    // Start is called before the first frame update
    public virtual void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        tf.position = target.position;
        Debug.Log(layerIndex);
    }

    protected virtual void OnDestroy()
    {
        Debug.Log(layerIndex);
        if (layerIndex == 0)
        {
            Debug.Log(orbitHandler.layer1Object.Remove(this));
        }
        else if (layerIndex == 1)
        {
            orbitHandler.layer2Object.Remove(this);
        }
        else if (layerIndex == 2)
        {
            orbitHandler.layer3Object.Remove(this);
        }

    }
}
