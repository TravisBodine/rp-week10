﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarPanel : OrbitObject
{
    public float EnergyRegainBuff;
    public override void Start()
    {
        base.Start();
        GameManager.instance.resourceHandler.regainRate += EnergyRegainBuff;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameManager.instance.resourceHandler.regainRate -= EnergyRegainBuff;
    }
}
