﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRandomizer : MonoBehaviour
{
    SpriteRenderer sr;

    //List<Sprite> PossiblePlanets;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();

        int planetIndex = Random.Range(1, 105);

        string fmt = "000.##";
        Debug.Log("planet_" + planetIndex.ToString(fmt));
        Sprite planetSprite = Resources.Load<Sprite>("planet_" + planetIndex.ToString(fmt));
        sr.sprite = planetSprite;
    }

  
}
